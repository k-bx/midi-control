//
// (c) 2020 Hubert Figuière
//
// License: LGPL-3.0-or-later

//! MIDI messages
//!

use crate::consts;
use crate::note::MidiNote;
use crate::sysex;
use thiserror::Error;

/// The MIDI Channel
#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
pub enum Channel {
    Ch1,
    Ch2,
    Ch3,
    Ch4,
    Ch5,
    Ch6,
    Ch7,
    Ch8,
    Ch9,
    Ch10,
    Ch11,
    Ch12,
    Ch13,
    Ch14,
    Ch15,
    Ch16,
    Invalid,
}

impl Channel {
    /// Convert the MIDI command byte to a Channel
    pub fn from_midi_cmd(v: u8) -> Channel {
        Self::from(v & consts::MIDI_CHANNEL_MASK)
    }
}

impl From<u8> for Channel {
    /// Convert a byte value 0-15 to the Channel.
    /// If you want to extract it from the MIDI event,
    /// use Channel::from_midi_cmd()
    fn from(v: u8) -> Channel {
        use Channel::*;
        match v {
            0 => Ch1,
            1 => Ch2,
            2 => Ch3,
            3 => Ch4,
            4 => Ch5,
            5 => Ch6,
            6 => Ch7,
            7 => Ch8,
            8 => Ch9,
            9 => Ch10,
            10 => Ch11,
            11 => Ch12,
            12 => Ch13,
            13 => Ch14,
            14 => Ch15,
            15 => Ch16,
            _ => Invalid,
        }
    }
}

/// Parameters of a key related event
///
/// This include note on and off and poly key pressure Value is
/// velocity for notes and pressure amount for pressure events.
#[derive(Clone, Debug, PartialEq)]
pub struct KeyEvent {
    /// The note to which this applies
    pub key: MidiNote,
    /// The velocity for notes or pressure value for pressure.
    pub value: u8,
}

/// Parameters for a control change
#[derive(Debug, PartialEq, Clone)]
pub struct ControlEvent {
    /// Control number
    pub control: u8,
    /// Value of the control
    pub value: u8,
}

/// The type of SysEx message
#[derive(Debug, PartialEq, Clone)]
pub enum SysExType {
    /// Manufacturer ID.
    Manufacturer(sysex::ManufacturerId),
    /// Non RealTime Universal SysEx
    /// Device ID, [sub id 1, sub id 2]
    NonRealTime(u8, [u8; 2]),
    /// Realtime Universal SysEx
    /// Device ID, [sub id 1, sub id 2]
    RealTime(u8, [u8; 2]),
}

/// SysEx message
#[derive(Debug, PartialEq, Clone)]
pub struct SysExEvent {
    /// Type of SysEx message
    pub r#type: SysExType,
    /// The raw data for the sysex. This include the terminating byte.
    pub data: Vec<u8>,
}

impl SysExEvent {
    /// Create a new SysEx message with a manufacturer ID
    /// * data is the data in the message, including the EOX
    pub fn new_manufacturer(manufacturer: sysex::ManufacturerId, data: &[u8]) -> SysExEvent {
        SysExEvent {
            r#type: SysExType::Manufacturer(manufacturer),
            data: Vec::from(data),
        }
    }

    /// Create a non realtime Universal System Exclusive message
    /// * device is the device. Use consts::usysex::ALL_CALL if you want all device to listen
    /// * subids is the two bytes for the message type.
    /// * data incude the rest of the data including EOX
    pub fn new_non_realtime(device: u8, subids: [u8; 2], data: &[u8]) -> SysExEvent {
        SysExEvent {
            r#type: SysExType::NonRealTime(device, subids),
            data: Vec::from(data),
        }
    }

    /// Create a realtime Universal System Exclusive message
    /// * device is the device. Use consts::usysex::ALL_CALL if you want all device to listen
    /// * subids is the two bytes for the message type.
    /// * data incude the rest of the data including EOX
    pub fn new_realtime(device: u8, subids: [u8; 2], data: &[u8]) -> SysExEvent {
        SysExEvent {
            r#type: SysExType::RealTime(device, subids),
            data: Vec::from(data),
        }
    }

    /// Get the SysEx type
    pub fn get_type(&self) -> &SysExType {
        &self.r#type
    }

    /// Get the data from the SysEx
    pub fn get_data(&self) -> &Vec<u8> {
        &self.data
    }
}

/// MIDI messages are what is being sent or received on the MIDI system
///
#[derive(Debug, PartialEq, Clone)]
pub enum MidiMessage {
    /// We don't know that message.
    Invalid,
    /// Undefined
    Undefined,
    /// Note on.
    NoteOn(Channel, KeyEvent),
    /// Note off.
    NoteOff(Channel, KeyEvent),
    /// Pressure for notes (aftertouch).
    PolyKeyPressure(Channel, KeyEvent),
    /// Control value changed.
    ControlChange(Channel, ControlEvent),
    /// Program change.
    ProgramChange(Channel, u8),
    /// Channel pressure.
    ChannelPressure(Channel, u8),
    /// Pitch bending. LSB and MSB of the change.
    PitchBend(Channel, u8, u8),
    /// System extension event.
    SysEx(SysExEvent),
    RtTimingClock,
    RtStart,
    RtContinue,
    RtStop,
    RtActiveSensing,
    RtSystemReset,
}

impl MidiMessage {
    /// Return the channel of the MIDI command
    /// This is a convenience helper to avoid having to destructure.
    /// Note: a SysEx message doesn't have a channel.
    pub fn get_channel(&self) -> Channel {
        use MidiMessage::*;
        match *self {
            Invalid | SysEx(_) | Undefined | RtTimingClock | RtStart | RtContinue | RtStop
            | RtActiveSensing | RtSystemReset => Channel::Invalid,
            NoteOn(ch, _)
            | NoteOff(ch, _)
            | PolyKeyPressure(ch, _)
            | ControlChange(ch, _)
            | ProgramChange(ch, _)
            | ChannelPressure(ch, _)
            | PitchBend(ch, _, _) => ch,
        }
    }

    /// Construct a SysEx message from the raw data.
    /// Will return an Invalid message of the data doesn't start
    /// with the SYSEX byte.
    fn sysex_message_from(data: &[u8]) -> MidiMessage {
        use consts::system_event::sysex::*;

        if data[0] != consts::SYSEX {
            return MidiMessage::Invalid;
        }

        let idx;
        let manufacturer = data[1];
        let r#type = match manufacturer {
            NON_REAL_TIME => {
                idx = 5;
                SysExType::NonRealTime(data[2], [data[3], data[4]])
            }
            REAL_TIME => {
                idx = 5;
                SysExType::RealTime(data[2], [data[3], data[4]])
            }
            _ => {
                let (_, d) = data.split_at(1);
                let manufacturer = sysex::ManufacturerId::from_raw(d).unwrap();
                idx = 1 + manufacturer.raw_len();
                SysExType::Manufacturer(manufacturer)
            }
        };
        MidiMessage::SysEx(SysExEvent {
            r#type,
            data: data.split_at(idx).1.to_vec(),
        })
    }
}

impl From<MidiMessage> for Vec<u8> {
    /// Convert the MidiMessage into a raw buffer suited to be sent,
    /// to the MIDI device.
    /// An empty vector mean nothing could be made.
    fn from(message: MidiMessage) -> Vec<u8> {
        use MidiMessage::*;
        match message {
            Undefined => vec![],
            NoteOff(ch, e) => vec![consts::NOTE_OFF | ch as u8, e.key, e.value],
            NoteOn(ch, e) => vec![consts::NOTE_ON | ch as u8, e.key, e.value],
            PolyKeyPressure(ch, e) => {
                vec![consts::POLYPHONIC_KEY_PRESSURE | ch as u8, e.key, e.value]
            }
            ControlChange(ch, e) => vec![consts::CONTROL_CHANGE | ch as u8, e.control, e.value],
            ProgramChange(ch, p) => vec![consts::PROGRAM_CHANGE | ch as u8, p, 0],
            ChannelPressure(ch, p) => vec![consts::CHANNEL_KEY_PRESSURE | ch as u8, p, 0],
            PitchBend(ch, lsb, msb) => vec![consts::PITCH_BEND_CHANGE | ch as u8, lsb, msb],
            RtTimingClock => vec![consts::RT_TIMING_CLOCK],
            RtStart => vec![consts::RT_START],
            RtContinue => vec![consts::RT_CONTINUE],
            RtStop => vec![consts::RT_STOP],
            RtActiveSensing => vec![consts::RT_ACTIVE_SENSING],
            RtSystemReset => vec![consts::RT_SYSTEM_RESET],
            SysEx(ref e) => {
                let out_size = match e.r#type {
                    SysExType::Manufacturer(m) => 1 + m.raw_len() + e.data.len(),
                    SysExType::NonRealTime(_, _) | SysExType::RealTime(_, _) => 5 + e.data.len(),
                };
                let mut vec = Vec::with_capacity(out_size);
                vec.push(consts::SYSEX);
                use SysExType::*;
                match e.r#type {
                    Manufacturer(ref b) => b.push_to(&mut vec),
                    NonRealTime(d, id) => {
                        vec.push(consts::system_event::sysex::NON_REAL_TIME);
                        vec.push(d);
                        vec.extend_from_slice(&id);
                    }
                    RealTime(d, id) => {
                        vec.push(consts::system_event::sysex::REAL_TIME);
                        vec.push(d);
                        vec.extend_from_slice(&id);
                    }
                }
                vec.extend_from_slice(&e.data);
                vec
            }
            Invalid => vec![],
        }
    }
}

impl From<&[u8]> for MidiMessage {
    /// Create a MidiMessage from raw data as received from the MIDI driver.
    fn from(data: &[u8]) -> MidiMessage {
        match from_consuming(data) {
            Ok((msg, _consumed)) => msg,
            Err(_) => MidiMessage::Invalid,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Error)]
pub enum ParsingError {
    #[error("Expecting at least {expected} bytes")]
    WrongLength { expected: usize },
    #[error("Unable to parse sequence: {sequence:?}")]
    UnknownSequence { sequence: Vec<u8> },
}

/// Create a MidiMessage from raw data as received from the MIDI driver, and return number of bytes consumed
pub fn from_consuming(data: &[u8]) -> Result<(MidiMessage, usize), ParsingError> {
    if data.len() == 0 {
        return Err(ParsingError::WrongLength { expected: 1 });
    } else {
        let (event, channel) = if data[0] < consts::SYSEX {
            (
                data[0] & consts::EVENT_TYPE_MASK,
                data[0] & consts::MIDI_CHANNEL_MASK,
            )
        } else {
            (data[0], 0u8)
        };
        match event {
            consts::NOTE_OFF => {
                let l = 3;
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                }
                // Note Off
                Ok((
                    MidiMessage::NoteOff(
                        Channel::from(channel),
                        KeyEvent {
                            key: data[1],
                            value: data[2],
                        },
                    ),
                    l,
                ))
            }
            consts::NOTE_ON => {
                let l = 3;
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                }
                // Note on with zero-velocity is a note off
                if data[2] == 0x00 {
                    Ok((
                        MidiMessage::NoteOff(
                            Channel::from(channel),
                            KeyEvent {
                                key: data[1],
                                value: data[2],
                            },
                        ),
                        l,
                    ))
                } else {
                    // Note On
                    Ok((
                        MidiMessage::NoteOn(
                            Channel::from(channel),
                            KeyEvent {
                                key: data[1],
                                value: data[2],
                            },
                        ),
                        l,
                    ))
                }
            }
            consts::POLYPHONIC_KEY_PRESSURE => {
                let l = 3;
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                };
                Ok((
                    MidiMessage::PolyKeyPressure(
                        Channel::from(channel),
                        KeyEvent {
                            key: data[1],
                            value: data[2],
                        },
                    ),
                    l,
                ))
            }
            consts::CONTROL_CHANGE => {
                let l = 3;
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                }
                Ok((
                    MidiMessage::ControlChange(
                        Channel::from(channel),
                        ControlEvent {
                            control: data[1],
                            value: data[2],
                        },
                    ),
                    l,
                ))
            }
            consts::PROGRAM_CHANGE => {
                let l = 2;
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                };
                Ok((
                    MidiMessage::ProgramChange(Channel::from(channel), data[1]),
                    l,
                ))
            }
            consts::CHANNEL_KEY_PRESSURE => {
                let l = 2;
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                }
                Ok((
                    MidiMessage::ChannelPressure(Channel::from(channel), data[1]),
                    l,
                ))
            }
            consts::PITCH_BEND_CHANGE => {
                let l = 3;
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                }
                Ok((
                    MidiMessage::PitchBend(Channel::from(channel), data[1], data[2]),
                    l,
                ))
            }
            consts::SYSEX => {
                let l = 5;
                // todo move out validation in inner `Result`
                if data.len() < l {
                    return Err(ParsingError::WrongLength { expected: l });
                }
                Ok((MidiMessage::sysex_message_from(data), l))
            }
            consts::channel_event::control_change::UNDEFINED_09 => Ok((MidiMessage::Undefined, 1)),
            consts::system_event::RT_TIMING_CLOCK => Ok((MidiMessage::RtTimingClock, 1)),
            consts::system_event::RT_START => Ok((MidiMessage::RtStart, 1)),
            consts::system_event::RT_CONTINUE => Ok((MidiMessage::RtContinue, 1)),
            consts::system_event::RT_STOP => Ok((MidiMessage::RtStop, 1)),
            consts::system_event::RT_ACTIVE_SENSING => Ok((MidiMessage::RtActiveSensing, 1)),
            consts::system_event::RT_SYSTEM_RESET => Ok((MidiMessage::RtSystemReset, 1)),
            0x00 => Ok((MidiMessage::Undefined, 1)),
            // TODO handle other system message
            _ => Err(ParsingError::UnknownSequence {
                sequence: data.to_vec().iter().take(5).map(|x| *x).collect(),
            }),
        }
    }
}

/// Iteratively call from_consuming until end of input
pub fn from_multi(data: &[u8]) -> Result<Vec<MidiMessage>, ParsingError> {
    let xs = from_consuming_multi(data)?;
    let mut res = vec![];
    for (a, _b) in xs {
        res.push(a);
    }
    Ok(res)
}

/// Like from_multi but filters out `Undefined` messages
pub fn from_multi_filtered(data: &[u8]) -> Result<Vec<MidiMessage>, ParsingError> {
    let xs = from_consuming_multi(data)?;
    let mut res = vec![];
    for (a, _b) in xs {
        if a != MidiMessage::Undefined {
            res.push(a);
        }
    }
    Ok(res)
}

/// Iteratively call from_consuming until end of input
pub fn from_consuming_multi(data: &[u8]) -> Result<Vec<(MidiMessage, usize)>, ParsingError> {
    let mut res: Vec<(MidiMessage, usize)> = vec![];
    let mut data2: Vec<u8> = data.clone().to_vec();
    while data2.len() > 0 {
        let (msg, consumed) = from_consuming(&data2)?;
        if consumed == 0 {
            return Ok(res);
        }
        res.push((msg, consumed));
        let mut i = consumed as i64;
        while i > 0 {
            data2.remove(0);
            i -= 1;
        }
    }
    Ok(res)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_midi_channel() {
        let ch = Channel::from_midi_cmd(128);
        assert_eq!(ch, Channel::Ch1);

        let ch = Channel::from_midi_cmd(137);
        assert_eq!(ch, Channel::Ch10);
    }

    #[test]
    pub fn test_midi_from_to_raw() {
        // Invalid
        let raw = vec![0u8];
        let msg = MidiMessage::Undefined;
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert!(out.len() == 0);

        // NoteOn
        let raw = vec![144u8, 59u8, 88u8];
        let msg = MidiMessage::NoteOn(Channel::Ch1, KeyEvent { key: 59, value: 88 });
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert_eq!(out, raw);

        // NoteOff
        let raw = vec![128u8, 60u8, 0u8];
        let msg = MidiMessage::NoteOff(Channel::Ch1, KeyEvent { key: 60, value: 0 });
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert_eq!(out, raw);

        // ControlChange
        let raw = vec![176, 114, 65];
        let msg = MidiMessage::ControlChange(
            Channel::Ch1,
            ControlEvent {
                control: 114,
                value: 65,
            },
        );
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert_eq!(out, raw);

        // PitchBend
        let raw = vec![224, 0, 76];
        let msg = MidiMessage::PitchBend(Channel::Ch1, 0, 76);
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert_eq!(out, raw);

        // SysEx
        let raw = vec![240, 30, 127, 66, 2, 0, 0, 16, 127, 247];
        let msg = MidiMessage::SysEx(SysExEvent::new_manufacturer(
            sysex::ManufacturerId::Id(30),
            &[127, 66, 2, 0, 0, 16, 127, 247],
        ));
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert_eq!(out, raw);

        let raw = vec![240, 0, 32, 107, 127, 66, 2, 0, 0, 16, 127, 247];
        let msg = MidiMessage::SysEx(SysExEvent::new_manufacturer(
            sysex::ManufacturerId::ExtId(32, 107),
            &[127, 66, 2, 0, 0, 16, 127, 247],
        ));
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert_eq!(out, raw);

        // Universal SysEx
        let raw = vec![240, 126, 0, 6, 2, 0, 32, 107, 2, 0, 4, 2, 67, 7, 0, 1, 247];
        let msg = MidiMessage::SysEx(SysExEvent::new_non_realtime(
            0,
            [6, 2],
            &[0, 32, 107, 2, 0, 4, 2, 67, 7, 0, 1, 247],
        ));
        assert_eq!(MidiMessage::from(raw.as_slice()), msg);
        let out: Vec<u8> = msg.into();
        assert_eq!(out, raw);
    }
}
