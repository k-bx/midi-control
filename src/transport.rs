//
// (c) 2020 Hubert Figuière
//
// License: LGPL-3.0-or-later

//! Transport for MIDI

use crate::MidiMessage;

/// Trait to implement sending messages
///
/// ## Example
///
/// Sending a Note On message:
///
/// ```no_run
/// use midi_control::{Channel,MidiMessage, KeyEvent};
/// use midi_control::transport::MidiMessageSend; // to use the trait
/// use midir;
///
/// let out = midir::MidiOutput::new("MIDITest").unwrap();
/// let port = &out.ports()[0];
/// if let Ok(mut midi_out) = out.connect(port, "MIDI device") {
///     let message = MidiMessage::NoteOn(Channel::Ch1, KeyEvent{key:60, value:127});
///     midi_out.send_message(message);
/// }
/// ```
pub trait MidiMessageSend {
    /// The error type to return on send.
    type Error;

    /// Send a `MidiMessage` over this transport layer.
    /// Return a `Self::Error` if any. For implementor of the trait this usually
    /// will be their own `Error` type.
    fn send_message(&mut self, message: MidiMessage) -> Result<(), Self::Error>;
}

#[cfg(feature = "transport")]
impl MidiMessageSend for midir::MidiOutputConnection {
    type Error = midir::SendError;

    /// Send a `MidiMessage` over a `midir::OutputConnection`
    fn send_message(&mut self, message: MidiMessage) -> Result<(), Self::Error> {
        let raw: Vec<u8> = message.into();
        self.send(raw.as_slice())
    }
}
